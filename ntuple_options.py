#########################################################################
# File Name: ntuple_.py
# Description: 
# Author: Yangyh
# mail: yangyh@ihep.ac.cn
# Created Time: Wed 23 Oct 2019 02:28:24 PM CEST
#########################################################################
#!/usr/bin/env python
from Configurables import DecayTreeTuple, DaVinci
from DecayTreeTuple.Configuration import *
from DaVinci.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool
from GaudiConf import IOHelper


def addNtupleToDaVinci(isSim):
    #theTuple = DecayTreeTuple('TupleLbToJpsipK_JpsiToee')
    theTuple = DecayTreeTuple('LbTuple')
    theTuple.TupleName = "Lb2JPK"
    theTuple.Decay = '[Lambda_b0 -> ^(J/psi(1S) -> ^e+ ^e-) ^(Lambda(1520)0 -> ^p+ ^K-)]CC'
    
    theTuple.addBranches ({
      "Lb" : "[Lambda_b0 -> (J/psi(1S) -> e+ e-) (Lambda(1520)0 -> p+ K-)]CC",
      "Jpsi" : "[Lambda_b0 -> ^(J/psi(1S) -> e+ e-) (Lambda(1520)0 -> p+ K-)]CC",
      "E1" : "[Lambda_b0 -> (J/psi(1S) -> ^e+ e-) (Lambda(1520)0 -> p+ K-)]CC",
      "E2" : "[Lambda_b0 -> (J/psi(1S) -> e+ ^e-) (Lambda(1520)0 -> p+ K-)]CC",
      "Proton" : "[Lambda_b0 -> (J/psi(1S) -> e+ e-) (Lambda(1520)0 -> ^p+ K-)]CC",
      "Kaon" : "[Lambda_b0 -> (J/psi(1S) -> e+ e-) (Lambda(1520)0 -> p+ ^K-)]CC"
      })


    theTuple.addTupleTool('TupleToolTrackInfo') 
    theTuple.addTupleTool('TupleToolBremInfo')
    theTuple.addTupleTool('TupleToolHelicity')
    theTuple.addTupleTool('TupleToolDOCA')
    if isSim:
        theTuple.addTupleTool('TupleToolMCTruth')
        theTuple.addTupleTool("TupleToolVtxIsoln")
    else:
        lineQ = "'Bu2LLK_eeLine2'"
        LoKi_Cone = LoKi__Hybrid__TupleTool("LoKi_Cone")
        LoKi_Cone.Variables = {
            #vertex variables
        "NumVtxWithinChi2WindowOneTrack": "RELINFO('/Event/Leptonic/Phys/'"+lineQ+
        "'/VertexIsoInfo', 'VTXISONUMVTX', -999.)",
        "SmallestDeltaChi2OneTrack": "RELINFO('/Event/Leptonic/Phys/'"+lineQ+
         "'/VertexIsoInfo', 'VTXISODCHI2ONETRACK', -999.)",
        "SmallestDeltaChi2MassOneTrack": "RELINFO('/Event/Leptonic/Phys/'"+lineQ+
        "'/VertexIsoInfo', 'VTXISODCHI2MASSONETRACK', -999.)",
        "SmallestDeltaChi2TwoTracks": "RELINFO('/Event/Leptonic/Phys/'"+lineQ+
        "'/VertexIsoInfo', 'VTXISODCHI2TWOTRACK', -999.)",
        "SmallestDeltaChi2MassTwoTracks": "RELINFO('/Event/Leptonic/Phys/'"+lineQ+
        "'/VertexIsoInfo', 'VTXISODCHI2MASSTWOTRACK', -999.)"
        }
        theTuple.Lb.addTool(LoKi_Cone , name = "LoKi_Cone" )
        theTuple.Lb.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_Cone"]
    TriggerList = [
        'L0ElectronDecision','L0HadronDecision',
        'Hlt1TrackMVADecision',
        'Hlt2Topo2BodyDecision','Hlt2Topo3BodyDecision','Hlt2Topo4BodyDecision'
       ]
    theTuple.addTupleTool('TupleToolTISTOS')
    theTuple.TupleToolTISTOS.TriggerList = TriggerList
    theTuple.TupleToolTISTOS.Verbose = True
    theTuple.Lb.addTupleTool('TupleToolSubMass')
    theTuple.Lb.TupleToolSubMass.Substitution += ["p+ => K+"]
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => p+/K-"]
    theTuple.Lb.addTupleTool( 'TupleToolDecayTreeFitter', name = "DTF" )
    theTuple.Lb.addTupleTool( theTuple.Lb.DTF.clone( "DTF_PV",constrainToOriginVertex = True ) )
    theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]
    theTuple.Lb.addTupleTool( theTuple.Lb.DTF.clone( "DTF_JPsi",constrainToOriginVertex = False,daughtersToConstrain = [ "J/psi(1S)" ] ) )
    theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi" ]
    theTuple.Lb.addTupleTool( theTuple.Lb.DTF.clone( "DTF_Lb",constrainToOriginVertex = False,daughtersToConstrain = [ "Lambda_b0" ],Verbose=True) )
    theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_Lb" ]
    
    LokiVariables = theTuple.addTupleTool('LoKi::Hybrid::TupleTool/LokiVariables')
    LokiVariables.Variables = {
        "ETA" : "ETA",
        "PHI" : "PHI",
        "VCHI2NDOF" : "VFASPF(VCHI2/VDOF)",
        "LOKI_MASS_JpsiConstr" : "DTF_FUN(M, True, 'J/psi(1S)')",
        "LOKI_MASS_LbConstr" : "DTF_FUN(M, True, 'Lambda_b0')" 
        }

    stream = 'Bu2KLL_NoPID.Strip'
    line = 'Bu2LLK_eeLine2'

    if isSim:
        theTuple.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(stream, line)]
    else: 
        DaVinci().RootInTES = '/Event/Leptonic'
        theTuple.Inputs = ['Phys/'+line+'/Particles']
        
        
    DaVinci().UserAlgorithms += [theTuple]

DaVinci().Simulation = False; #or False for data
DaVinci().InputType = "MDST" #for Data
DaVinci().Lumi = False;
if DaVinci().Simulation:
    DaVinci().InputType = "DST"  # for MC
    DaVinci().Lumi = False # for MC

Year = '2016'
MagnetPolarity = 'MD'

DaVinci().DataType = Year
DaVinci().EvtMax = 20000 # -1 to run over the full sample
DaVinci().TupleFile = "Lambdab.root"


if DaVinci().Simulation:
    DaVinci().DDDBtag = 'dddb-20170721-3'
    CondDB_tag = 'sim-20170721-2-vc'
    if MagnetPolarity == 'MU':
        DaVinci().CondDBtag = CondDB_tag+"-mu100"
    elif MagnetPolarity == 'MD':
        DaVinci().CondDBtag = CondDB_tag+"-md100"

addNtupleToDaVinci(DaVinci().Simulation)

if DaVinci().Simulation:
    IOHelper().inputFiles(["/eos/user/y/yoyang/MC/lambda2pkjpsi/00068070_00000081_1.bu2kll_nopid.strip.dst"],clear=True)
else:
    IOHelper().inputFiles(["/eos/user/y/yoyang/Data/00069527_00000003_1.leptonic.mdst"], clear=True)


from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
        STRIP_Code = "HLT_PASS_RE('StrippingBu2LLK_eeLine2Decision')"
        )
DaVinci().EventPreFilters = fltrs.filters('Filters')
#theTuple.Decay = '[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC'
#theTuple.setDescriptorTemplate('${Dstar}[D*(2010)+ -> ${D0}(D0 -> ${Kminus}K- ${Kplus}K+) ${pisoft}pi+]CC')
#theTuple.setDescriptorTemplate('${Lb}[Lambda_b0 -> ${proton}p+ ${Kminus}K- ${J}(J/psi(1S) -> ${eplus}e- ${eminus}e+)]CC')
#
#track_tool = theTuple.addTupleTool('TupleToolTrackInfo')
#track_tool.Verbose = True
#theTuple.addTupleTool('TupleToolPrimaries')
#lb_hybrid = theTuple.Lb.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Lb")
#proton_hybrid = theTuple.proton.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_proton')
#kminus_hybrid = theTuple.kminus.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Kminus')
#j_hybrid = theTuple.J.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_J')
#
#preamble = [
#        'DZ = VFASPF(VZ) - BPV(VZ)',
#            'TRACK_MAX_PT = MAXTREE(ISBASIC & HASTRACK, PT, -1)'
#            ]
#lb_hybrid.Preambulo = preamble
#j_hybrid.Preambulo = preamble
#kminus_hybrid.Preambulo = preamble
#proton_hybrid.Preambulo = preamble
#
#lb_hybrid.Variables = {
#    'mass' : 'M',
#    'mass_J' : 'CHILD(M, 1)',
#    'dz' : 'DZ'
#    }
#
#d0_hybrid.Variables = {
#   'mass': 'M',
#    'pt': 'PT',
#    'dira': 'BPVDIRA',
#    'vtx_chi2': 'VFASPF(VCHI2)',
#    'dz': 'DZ' 
#    }
#
#pisoft_hybrid.Variables = {
#    'p': 'P',
#    'pt': 'PT'
#}
#
#theTuple.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConstVtx')
#theTuple.Dstar.ConstVtx.constrainToOriginVertex = True
#theTuple.Dstar.ConstVtx.daughtersToConstrain = []
#
#
#
#
#theTuple.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConstD')
#theTuple.Dstar.ConstD.constrainToOriginVertex = True
#theTuple.Dstar.ConstD.Verbose = True
#theTuple.Dstar.ConstD.daughtersToConstrain = ['D0']
#theTuple.Dstar.ConstD.UpdateDaughters = True
#
#
#theTuple.Dstar.addTupleTool('TupleToolDecayTreeFitter/ConstDKpi')
#theTuple.Dstar.ConstDKpi.constrainToOriginVertex = True
#theTuple.Dstar.ConstDKpi.Verbose = True
#theTuple.Dstar.ConstDKpi.daughtersToConstrain = ['D0']
#
#from Configurables import DaVinci
#DaVinci().UserAlgorithms += [theTuple]
#DaVinci().InputType = 'DST'
#DaVinci().TupleFile = 'DVntuple.root'
#DaVinci().PrintFreq = 1000
#DaVinci().DataType = '2016'
#DaVinci().Simulation = True
#DaVinci().Lumi = not DaVinci().Simulation
#DaVinci().EvtMax = -1
#DaVinci().CondDBtag = 'sim-20170721-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20170721-3'
#
##FOR REAL DATA ONLY!
##from Configurables import CondDB
##ConDB(LatestGlobalTagByDataType = '2016')
#
##Inputs!
#
#from GaudiConf import IOHelper
#
#IOHelper().inputFiles([
#        './00070793_00000001_7.AllStreams.dst'
#        ], clear=True)
#
#from PhysConf.Filters import LoKi_Filters
#fltrs = LoKi_Filters(
#        STRIP_Code = "HLT_PASS_RE('StrippingD2hhPromptDst2D2KKLineDecision')")
#
#DaVinci().EventPreFilters = fltrs.filters('Filters')
