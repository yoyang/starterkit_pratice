#########################################################################
# File Name: first.py
# Description: 
# Author: Yangyh
# mail: yangyh@ihep.ac.cn
# Created Time: Wed 23 Oct 2019 01:38:47 PM CEST
#########################################################################
#!/usr/bin/env python
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import DaVinci
from GaudiConf import IOHelper

dv = DaVinci()
dv.DataType = '2016'
dv.Simulation = True

#Pass a file from the command line
inputFiles = [sys.argv[-1]]
IOHelper('ROOT').inputFiles(inputFiles)

appMgr = GP.AppMgr()
evt = appMgr.evtsvc()

appMgr.run(1)
evt.dump()

def nodes(evt, node=None):
    
        nodenames = []

        if node is None:
                root = evt.retrieveObject('')
                node = root.registry()

        if node.object():
                nodenames.append(node.identifier())
                for l in evt.leaves(node):
                        if 'Swum' in l.identifier():
                                continue
                        temp = evt[l.identifier()]
                        nodenames += nodes(evt, l)
        else:
                nodenames.append(node.identifier())
        return nodenames

def advance(decision):
    
        n = 0
        while True:
                appMgr.run(1)
                if not evt['/Event/Rec/Header']:
                        print "We're at the end!"
                        break
                n += 1
                dec=evt['/Event/Strip/Phys/DecReports']
                if dec.hasDecisionName('Stripping{0}Decision'.format(decision)):
                        break
        return n

