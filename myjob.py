#########################################################################
# File Name: myjob.py
# Description: 
# Author: Yangyh
# mail: yangyh@ihep.ac.cn
# Created Time: Mon 18 Nov 2019 01:21:57 PM CET
#########################################################################
#!/usr/bin/env python
taskname = 'Lb16'
#simulation sample (ignore line break in the path)
data ='/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28Filtered/15154001/BU2KLL_NOPID.STRIP.DST'
#real data (ignore line break in the path)
#data='/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real_Data/Reco16/Stripping28r1/90000000/LEPTONIC.MDST'
myApplication = GaudiExec()
myApplication.directory = './DaVinciDev_v45r3/'
myApplication.options = ['ntuple_options.py']
myApplication.platform = 'x86_64-centos7-gcc8-opt'

ds = BKQuery(data, dqflag = "All").getDataset()
print(ds)
#print ds.getDataset()
j = Job(
      name = taskname,
      backend = Dirac(),
      application = myApplication,
      splitter = SplitByFiles(
      filesPerJob = 6,
      ignoremissing = True
      ),
    outputfiles = [DiracFile('*.root'),],
    inputdata = ds
    )
j.submit()
