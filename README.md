# Starterkit_pratice

Configure:  lxplus computing system

We gonna do  lambda_b -> p K Jpsi(e+e-)

1.find proper Stripping version
http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/
choose Stripping 28r1 -> Leptonic->StrippingBu2LLK_eeLine2

2.find the MC sample and Data
1) eventtype: 15154001
   link: https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/blob/v30r38/dkfiles/Lb_JpsipK,ee=phsp,DecProdCut.dec
2) goto https://lhcb-portal-dirac.cern.ch/DIRAC
    change "Simulation" selection to "eventtype"
    then choose "MC->2016->15154001->Sim9c->Trig0x6138160F->Reco16->Turbo03>Stripping28Filtered/BU2KLL_NOPID.STRIP.DST
    save file with name "MC_2016_15154001_Beam6500GeV2016MagDownNu1.625nsPythia8_Sim09c_Trig0x6138160F_Reco16_Turbo03_Stripping28Filtered_BU2KLL_NOPID.STRIP.DST.py" 

3)LHCb->Collision16->90000000->Beam6500GeV-VeloClosed-MagDown->Real Data->Reco16->Stripping28r1->LEPTONIC.MDST
the save file as "LHCb_Collision16_90000000_Beam6500GeVVeloClosedMagDown_Real Data_Reco16_Stripping28r1_LEPTONIC.MDST.py"
    

3.Download MC and Data sample

`> lhcb-proxy-init`

`> lb-run LHCbDIRAC dirac-dms-get-file --File=filename.py #down all dst files record in the filename`

`> lb-run LHCbDIRAC dirac-dms-get /lhcb/MC/2016/ALLSTREAMS.DST/00070793/0000/00070793_00000574_7.AllStreams.dst #only down one dst file`

`> lb-run LHCbDIRAC dirac-dms-lfn-accessURL /lhcb/MC/2016/ALLSTREAMS.DST/00070793/0000/00070793_00000574_7.AllStreams.dst #If the file above can not be found, use this command to obtain URL of this file's backup, and then download it with previous command`

3.configure DaVinci environment 

Here we use DaVinci v45r3

check the platform compatible with DaVinci v45r3

`> lb-sdb-query listPlatforms DaVinci v45r3`

`> export CMTCONFIG=x86_64-centos7-gcc8-opt` # set platform 

open the DST and print out some of the TES locations which exist for this event. 

`> lb-run DaVinci/v45r3 ipython -i first.py 00068070_00000093_1.bu2kll_nopid.strip.dst`

`[0]: tracks = evt['/Event/Rec/Track/Best'] #obtain the address of track`

`[1]: print tracks[0] # print information about no.1 track`

`[2]: nodes(evt)` #list a large number of TES locations, some locations are ‘packed’, for example: /Event/AllStreams/pPhys/Particles. You can not access these directly at this location. Instead you have to know what location the contents will get unpacked to when you want to use it. Often you can just try removing the small p from the location (/Event/AllStreams/Phys/Particles).

`[3]: line = 'Bu2LLK_eeLine2'`

`[4]: advance(line) # when decision of stripping line is true, return  candidate`

`[5]: cands = evt['/Event/AllStreams/Phys/{0}/Particle'.format(line)]` 

`[6]: print cands[0]`   # You can test if line exists in the sample 

`[5]: quit()`

create the enviroment 

`> lb-dev DaVinci/v45r3`

`> cd ./DaVinciDev_v45r3`

`> git lb-use DaVinci `

We already the version of DaVinci, there should a comptible version of Analysis, which can be found in the link

http://lhcbdoc.web.cern.ch/lhcbdoc/davinci/releases/v45r3/. main page of lhcb -> PROJECTS -> Computing and Software -> DaVinciAnalysis framework -> release notes -> v45r3

`> git lb-use Analysis`

`> git lb-checkout Analysis/v21r3 Phys/DecayTreeTuple`

`> make configure`

`> make`

`> ./run gaudirun.py` #check if you can run it

you already the TES location with nodes(evt) "/Event/Bu2KLL_NoPID.Strip/Phys/Bu2LLK_eeLine2/Particles"
which could be used in the "DecayTreeTuple"
 
4. Check your option file before submit
1) Header of Python
   ` from Configurables import DaVinci, DecayTreeTuple
    from DaVinci.Configuration import *
    from DecayTreeTuple.Configuration import *
    from GaudiConf import IOHelper`

2) Write the descriptor in the stripping line

    `theTuple.Decay = '[ Lambda_b0 -> ^(J/psi(1S) -> ^e+ ^e-) ^(Lambda(1520)0 -> ^p+ ^K-)]CC'`
    
3) define line

    `line = 'Bu2LLK_eeLine2'
    if isSim: # For MC DST
        theTuple.Inputs = ['/Event/Bu2KLL_NoPID.Strip/Phys/{0}/Particles'.format(line)]
    else:  #For data MDST
        DaVinci().RootInTES = "/Event/Leptonic"
        theTuple.Inputs = ['Phys/'+line+'/Particles']  `
        
 
`> ./run bash -l`

`> ./run gaudirun.py ntuple_opions.py`


Download new TupleTool and compile

`> exit` # exit the environment

`> git clone ssh://git@gitlab.cern.ch:7999/vlisovsk/ttps.git`

`> mv ttps/* Phys/DecayTreeTuple/src/`

`> make`

`> ./run bash -l`

`> ./run gaudirun.py ntuple_opions.py`

subjob

`> ganga myjob.py`

check jobs

To be continue
